using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    private Rigidbody2D rigidbody;
    private SpriteRenderer sprite;
    
    public bool on_ground;
    public Transform ground_check;
    public float check_radius = 0.5;
    public LayerMask ground;

    [SerializeField] private float jumpforce;
    [SerializeField] private float speed;
    
    public void CheckingGround() {
        on_ground = Physics2D.OverlapCircle(ground_check.position, check_radius, ground);
    }
    
    public void Jump()
    {
        if (on_ground == true)
            rigidbody.AddForce(Vector2.up * jumpforce, ForceMode2D.Impulse);
    }

    private void Move()
    {
        Vector3 dir = transform.right * Input.GetAxis("Horizontal");
        transform.position = Vector3.MoveTowards(transform.position, transform.position + dir, speed * Time.deltaTime);

        sprite.flipX = dir.x > 0.0f;
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Horizontal"))
        {
            Move();
        }

        if (Input.GetButton("Jump"))
        {
            Jump();
        }

        CheckingGround();
    }
}
